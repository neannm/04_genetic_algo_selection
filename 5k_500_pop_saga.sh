#!/bin/bash

# Job name:
#SBATCH --job-name=5k_2_runs
#
# Project:
#SBATCH --account=nn9643k
#
# Wall clock limit:
#SBATCH --time=5-00:00:00
#
# Max memory usage:
#SBATCH --mem-per-cpu=4G

#numer of task
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10

## Create and move to work dir
workdir=$USERWORK/$SLURM_JOB_ID
mkdir -p $workdir


## Set up job environment:
module purge
module load Anaconda3/2019.03

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/cluster/software/Anaconda3/2019.03/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/cluster/software/Anaconda3/2019.03/etc/profile.d/conda.sh" ]; then
        . "/cluster/software/Anaconda3/2019.03/etc/profile.d/conda.sh"
    else
        export PATH="/cluster/software/Anaconda3/2019.03/bin:$PATH"
    fi
fi
unset __conda_setup


conda activate rdkit

#export CHEMFP_LICENSE=20210218-KirchmairGroup@ODHBMPGHAPKFNCBFDNJJOGMAJNGCCJHJ

set -o errexit # exit on errors


## Copy input files to the work directory:
cp ga7_novelty_pfams_parallel_saga.py $workdir
cp ../data_library_creation/genetic_algo_second_round_inputs/query_with_targets.csv $workdir
cp ../data_library_creation/genetic_algo_second_round_inputs/targets_with_novelty_scores.csv $workdir


## Do some work:
cd $workdir
mkdir Results


python ga7_novelty_pfams_parallel_saga.py 5000 500 300 10 10

##copy results back to home
cp -a Results /cluster/home/nmathai/library_creation2/04_genetic_algo_selection/


