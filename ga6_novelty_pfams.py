import pandas.io.sql as psql
import json
from rdkit.Chem import AllChem as Chem
from rdkit.Chem import Descriptors
#from rdkit.Chem.Draw import IPythonConsole
from rdkit.Chem import PandasTools
from rdkit.Chem import Draw
from rdkit import rdBase
from rdkit import DataStructs
#import psycopg2 as pg


import pandas as pd
import sqlalchemy as db
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from pandas import concat

from collections import OrderedDict
import requests
import numpy as np
import matplotlib.pyplot as plt
import joblib
from sklearn.model_selection import train_test_split

import cheminformatics.utils.multicore as multicore
import time
import math
from multiprocessing import Pool,Queue, Process, cpu_count
import os
import sys

import chemfp
from chemfp import search

import numpy as np

import random
from numpy import median 
from itertools import combinations

from rdkit.Chem import QED 
from collections import Counter


#fitness score calculation ............
def fitness_function(population, targets_families) :

    fitness_scores = [] # simple sum of all the other scores

    #print(targets_families.columns)
    for idx_chr, chromosome in enumerate(population):
        
        chromo_size = len(chromosome)
        predictions = pd.DataFrame(columns=['target','query_index'])
        
        #get the compounds which were selected
        #print(idx_chr)
        temp_df = full_set[full_set.index.isin(chromosome)][['query_smiles','query_index','targets']]
        
        list_of_targets = temp_df.targets.tolist()
        
        #go through each of the lists of targets and add to the set, ie for eacg mol in the chromo
        for indx_mol, indi_list in enumerate(list_of_targets):
            #strip
            indi_list = str(indi_list).strip('{ }').split(',')
            indi_list = pd.DataFrame(indi_list, columns=['target'])
            indi_list['query_index'] =  temp_df.iloc[indx_mol]['query_index']
            predictions = pd.concat([predictions,indi_list], ignore_index=True)

        predictions= pd.merge(predictions,targets_families, right_on='target_chembl_id',left_on='target',how='left')
        
        #print(predictions.columns)
        predictions = predictions[['query_index','pfam','pfam_novelty_score']]
        
        predictions.drop_duplicates(inplace = True)
        
        grouped_predictions = pd.DataFrame({'count' : predictions.groupby( [ "pfam", "pfam_novelty_score"]).size()}).reset_index()

        grouped_predictions['score'] = grouped_predictions['pfam_novelty_score']*((1 -np.power(0.99,grouped_predictions['count'])) / (1- 0.99))

        fitness_scores.append((grouped_predictions['score'].sum())/10000)
    return fitness_scores



#adapted from:https://towardsdatascience.com/genetic-algorithm-implementation-in-python-5ab67bb124a6
def select_mating_pool(pop, fitness_scores, num_parents):
    
    parents = []

    for parent_num in range(num_parents):
        #get index with max fitness
        max_fitness_idx = np.where(fitness == np.max(fitness_scores))
        max_fitness_idx = max_fitness_idx[0][0]
        
        #add that to parents
        parents.append(pop[max_fitness_idx])
        
        #remove from consideration
        fitness[max_fitness_idx] = -99999999999

    return(parents)


# crossover for novelty implementation where randomness is added to a chromosome and not a population
def crossover_and_mutate(parents, offspring_size, full_pop, perc_random_genes):
    
    offsprings = []
    # The point at which crossover takes place between two parents. Usually, it is at the center.
    chromosome_size = len(parents[0])
    crossover_point = int(chromosome_size/2)
    num_genes_inherited = round(chromosome_size*(1-perc_random_genes)) 

    k = 0 
    
    #while we still need offsprints
    while len(offsprings) < offspring_size :
        # pick parents: 
        
        # Index of the first parent to mate.
        parent1_idx = k
        # Index of the second parent to mate.
        parent2_idx = 0 if (k+1) == len(parents) else (k+1) #loop back to 0 if the last parent is reached
        
        # four offspring parts from 2 parents
        offspring_a = list(parents[parent1_idx][0:crossover_point]) #half from first parent
        offspring_b = list(parents[parent1_idx][crossover_point:]) #2nd half from first parent
        offspring_c = list(parents[parent2_idx][0:crossover_point]) #half from second parent
        offspring_d = list(parents[parent2_idx][crossover_point:]) #2nd half from second parent
        
        # resulting in 4 offsprings (since AC = CA)
        offspring1 =  offspring_a + offspring_c 
        offspring2 =  offspring_a + offspring_d 
        offspring3 =  offspring_b + offspring_c 
        offspring4 =  offspring_b + offspring_d 
        

        #for each offspring
        for offspring in [offspring1,offspring2,offspring3,offspring4]:
            
            #print(len(offspring))
            
            #remove any duplicate compounds
            offspring = (list(set(offspring)))
                      
            #inherit only a certain number of genes
            if len(offspring)>num_genes_inherited:
                offspring = (random.sample(offspring, num_genes_inherited))

            #how many more compounds needed after duplicate removal
            num_needed_compounds = chromosome_size - len(offspring)
        
            #how many more compounds needed after duplicate removal
            if (num_needed_compounds) > 0:

                #indeces that are not already a gene
                selection_pool = set(full_pop) - set(offspring)

                #add the x needed randomly selected compounds to the offspring
                offspring = offspring + list(random.sample(selection_pool, num_needed_compounds))

            #add offspring to offsprings
            offsprings.append(offspring)
            
        #increment k
        k = k +1 
    
    #get the first needed number of offsprings from the offspring list. 
    #There may have been more at this point as not all 4 offsprings from the last pair may have been needed
    offsprings = offsprings[0:offspring_size]

    
    return offsprings


full_set = pd.read_csv("../03_target_pred/Results/query_with_targets.csv",encoding='utf-8')

targets_families = pd.read_csv("../01_chembl_data_prep/Results/targets_with_novelty_scores.csv",encoding='utf-8')


#initialize population
chromosome_size = int(sys.argv[1]) ## number of individual query molecules to collect
population_size = int(sys.argv[2]) ## number of sets of query molecules
number_of_generations = int(sys.argv[3]) ##number of generations
prop_random = int(sys.argv[4]) # proprtion of randoms for every gen

best_score = -999999.999999 #best performing chromosome
best_chromosome = []
gen_producing_best = -99

parent_size = round(((population_size))/3)
children_size = round(((population_size))*2/3)


print(parent_size, children_size)

out_put_filename =  "novel6_elitist_pfam_novelty_"+str(int(chromosome_size/1000))+"k"+"_"+str(population_size)+"_"+str(number_of_generations)+"_"+str(prop_random)+"randoms"
print(out_put_filename)
os.mkdir('Results/'+str(out_put_filename))


#create an initial population with an x chromosomes each of which has 10k unique queries (ie genes)
population =([(np.random.choice(full_set.shape[0], chromosome_size, replace=False)) for i in range(population_size)])

print("chromosome:"+str(chromosome_size)+", population: "+str(population_size)+", generations "+str(number_of_generations))

for generation in range(number_of_generations):
    
    #print("\n \n Generation", generation)
    
    #measure fitness  
    fitness = fitness_function(population,targets_families)
    
    #convert measures to dataframe
    result = pd.DataFrame({'fitness':fitness })
    result['generation'] = generation
    
    #get best performer so far
    if(max(fitness)>best_score):
        best_score = max(fitness) 
        best_chromosome = population[np.where(fitness == max(fitness))[0][0]]
        gen_producing_best = generation
        print("Best score so far:", best_score," from ",generation)
        np.savetxt('Results/'+str(out_put_filename)+'/'+str("best_chromo_"+out_put_filename+"_"+str(generation)+".csv"), best_chromosome, delimiter=",")
        
    #select parents
    parents = select_mating_pool(population, fitness,parent_size )
    #print("Done selecting from generation", generation)    
    # generate offspring
    children = crossover_and_mutate(parents, children_size, list(range(0,full_set.shape[0])) ,prop_random/100 )

   
    
    #randoms = ([(np.random.choice(full_set.shape[0], chromosome_size, replace=False)) for i in range(int(population_size*(prop_random/100)))])

    #print("Done creating children generation", generation)
    #create new population:
    population = parents + children  #+ randoms
    
    
    print(generation, "done")     

    if not os.path.isfile('Results/'+str(out_put_filename)+'/'+str("result_"+out_put_filename+".csv")):
        print("in here")
        result.to_csv('Results/'+str(out_put_filename)+'/'+str("result_"+out_put_filename+".csv"), index=False)
               
    else:
        result.to_csv('Results/'+str(out_put_filename)+'/'+str("result_"+out_put_filename+".csv"), index=False, mode='a', header=False)


