import pandas.io.sql as psql
import json
from rdkit.Chem import AllChem as Chem
from rdkit.Chem import Descriptors
#from rdkit.Chem.Draw import IPythonConsole
from rdkit.Chem import PandasTools
from rdkit.Chem import Draw
from rdkit import rdBase
from rdkit import DataStructs
#import psycopg2 as pg


import pandas as pd
import sqlalchemy as db
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from pandas import concat

from collections import OrderedDict
import requests
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

import cheminformatics.utils.multicore as multicore
import time
import math
from multiprocessing import Pool,Queue, Process, cpu_count
import os
import sys

import chemfp
from chemfp import search

import numpy as np

import random
from numpy import median 
from itertools import combinations

from rdkit.Chem import QED 
from collections import Counter


#fitness score calculation ............
def part_fitness_func(awks):
    
    chromosome, targets_families, full_set = awks
    
    chromo_size = len(chromosome)
    predictions = pd.DataFrame(columns=['target','query_index'])

    #get the compounds which were selected
    #print(idx_chr)
    temp_df = full_set[full_set.index.isin(chromosome)][['query_smiles','query_index','targets']]

    list_of_targets = temp_df.targets.tolist()

    #go through each of the lists of targets and add to the set, ie for eacg mol in the chromo
    for indx_mol, indi_list in enumerate(list_of_targets):
        #strip
        indi_list = str(indi_list).strip('{ }').split(',')
        indi_list = pd.DataFrame(indi_list, columns=['target'])
        indi_list['query_index'] =  temp_df.iloc[indx_mol]['query_index']
        predictions = pd.concat([predictions,indi_list], ignore_index=True)

    predictions= pd.merge(predictions,targets_families, right_on='target_chembl_id',left_on='target',how='left')

    #print(predictions.columns)
    predictions = predictions[['query_index','pfam','pfam_novelty_score']]

    predictions.drop_duplicates(inplace = True)

    grouped_predictions = pd.DataFrame({'count' : predictions.groupby( [ "pfam", "pfam_novelty_score"]).size()}).reset_index()

    grouped_predictions['score'] = grouped_predictions['pfam_novelty_score']*((1 -np.power(0.99,grouped_predictions['count'])) / (1- 0.99))

    return((grouped_predictions['score'].sum())/chromo_size)

def fitness_function(population, targets_families, full_set, num_cores) :

    fitness_scores = [] # simple sum of all the other scores

    with Pool(num_cores) as p:
        
        generator = ([chromosome, targets_families,full_set] for chromosome in population)
        
        fitness_scores = p.map(part_fitness_func, generator)
        

            
    return fitness_scores 

#adapted from:https://towardsdatascience.com/genetic-algorithm-implementation-in-python-5ab67bb124a6
def select_mating_pool(pop, fitness_scores, num_parents):
    
    parents = []

    for parent_num in range(num_parents):
        #get index with max fitness
        max_fitness_idx = np.where(fitness_scores == np.max(fitness_scores))
        max_fitness_idx = max_fitness_idx[0][0]
        
        #add that to parents
        parents.append(pop[max_fitness_idx])
        
        #remove from consideration
        fitness_scores[max_fitness_idx] = -99999999999

    return(parents)


def crossover_and_mutate(parents, offspring_size, full_pop):
    
    offsprings = []
    # The point at which crossover takes place between two parents. Usually, it is at the center.
    chromosome_size = len(parents[0])
    crossover_point = int(chromosome_size/2)
    num_genes_inherited = round(chromosome_size*(0.90)) # inherit 90% genes, 10% new compounds 

    k = 0 
    
    #while we still need offsprints
    while len(offsprings) < offspring_size :
        # pick parents: 
        
        # Index of the first parent to mate.
        parent1_idx = k
        # Index of the second parent to mate.
        parent2_idx = 0 if (k+1) == len(parents) else (k+1) #loop back to 0 if the last parent is reached
        
        # four offspring parts from 2 parents
        offspring_a = list(parents[parent1_idx][0:crossover_point]) #half from first parent
        offspring_b = list(parents[parent1_idx][crossover_point:]) #2nd half from first parent
        offspring_c = list(parents[parent2_idx][0:crossover_point]) #half from second parent
        offspring_d = list(parents[parent2_idx][crossover_point:]) #2nd half from second parent
        
        # resulting in 4 offsprings (since AC = CA)
        offspring1 =  offspring_a + offspring_c 
        offspring2 =  offspring_a + offspring_d 
        offspring3 =  offspring_b + offspring_c 
        offspring4 =  offspring_b + offspring_d 
        

        #for each offspring
        for offspring in [offspring1,offspring2,offspring3,offspring4]:
            
            #print(len(offspring))
            
            #remove any duplicate compounds
            offspring = (list(set(offspring)))
            
            #inherit only a certain number of genes
            if len(offspring)>num_genes_inherited:
                offspring = (random.sample(offspring, num_genes_inherited))
                
            #how many more compounds needed after duplicate removal
            num_needed_compounds = chromosome_size - len(offspring)
            if num_needed_compounds > 0:

                #indeces that are not already a gene
                selection_pool = set(full_pop) - set(offspring)

                #add the x needed randomly selected compounds to the offspring
                offspring = offspring + list(random.sample(selection_pool, num_needed_compounds))

            # shuffle offspring
            #random.shuffle(offspring)
            
            #add offspring to offsprings
            offsprings.append(offspring)
            
        #increment k
        k = k +1 
    
    #get the first needed number of offsprings from the offspring list. 
    #There may have been more at this point as not all 4 offsprings from the last pair may have been needed
    offsprings = offsprings[0:offspring_size]

    
    return offsprings


full_set_read = pd.read_csv("../03_target_pred/Results/query_with_targets.csv",encoding='utf-8')

targets_families_read = pd.read_csv("../01_chembl_data_prep/Results/targets_with_novelty_scores.csv",encoding='utf-8')


#initialize population
chromosome_size_set = int(sys.argv[1]) ## number of individual query molecules to collect
population_size_set = int(sys.argv[2]) ## number of sets of query molecules
generation_start_set = int(sys.argv[3]) ##what generation to start on
number_of_generations_set = int(sys.argv[4]) ##number of generations
prop_random_set = int(sys.argv[5]) # proprtion of randoms for every gen
num_cores_set = int(sys.argv[6]) # proprtion of randoms for every gen

best_score_set = -999999.999999 #best performing chromosome
best_chromosome_set = []
gen_producing_best_set = -99

parent_size_set = round(((population_size_set)-(population_size_set*(prop_random_set/100)))/3)
children_size_set = round(((population_size_set)-(population_size_set*(prop_random_set/100)))*2/3)

print(parent_size_set, children_size_set, prop_random_set)

out_put_filename =  "novel7_elitist_pfam_novelty_parallel_"+str(int(chromosome_size_set/1000))+"k"+"_"+str(population_size_set)+"_"+str(number_of_generations_set)+"_"+str(prop_random_set)+"randoms"
print(out_put_filename)


#create an initial population with an x chromosomes each of which has 10k unique queries (ie genes)
if(generation_start_set==0):
    population_set = [list(np.random.choice(full_set_read.shape[0], chromosome_size_set, replace=False)) for i in range(population_size_set)]
    os.mkdir('Results/'+str(out_put_filename))
else:
    population_set = pd.read_csv('Results/'+str(out_put_filename)+'/'+str("best_population_so_far"+out_put_filename+".csv"), header=None) #read the best population
    population_set = population_set.astype('int')
    population_set = population_set.to_numpy()



print("chromosome:"+str(chromosome_size_set)+", population: "+str(population_size_set)+", generations "+str(number_of_generations_set))

for generation_set in range(generation_start_set, number_of_generations_set):
    
    #print("\n \n Generation", generation)
    
    #measure fitness  
    fitness_set = fitness_function(population_set,targets_families_read, full_set_read, num_cores_set)
    
    #convert measures to dataframe
    result_set = pd.DataFrame({'fitness':fitness_set })
    result_set['generation'] = generation_set
    
    #get best performer so far
    max_fitness_this_gen  = max(fitness_set)
    if(max_fitness_this_gen>best_score_set):
        best_score_set = max_fitness_this_gen
        best_chromosome_set = population_set[np.where(fitness_set == max_fitness_this_gen)[0][0]]
        gen_producing_best_set = generation_set
        print("Best score so far:", best_score_set," from ",generation_set)
        np.savetxt('Results/'+str(out_put_filename)+'/'+str("best_chromo_"+out_put_filename+"_"+str(generation_set)+".csv"), best_chromosome_set, delimiter=",")
        np.savetxt('Results/'+str(out_put_filename)+'/'+str("best_population_so_far"+out_put_filename+".csv"), population_set, delimiter=",")
        
    #select parents
    parents_set = select_mating_pool(population_set, fitness_set,parent_size_set)
    #print("Done selecting from generation", generation)    
    # generate offspring
    children_set = crossover_and_mutate(parents_set, children_size_set, list(range(0,full_set_read.shape[0])))
    
    #create new population with only children and parents:
    population_set = parents_set + children_set
    
    #only get unique choromosomes
    population_set = [list(x) for x in set(tuple(x) for x in population_set)]

    #get the randoms needed
    randoms_set = [list(np.random.choice(full_set_read.shape[0], chromosome_size_set, replace=False) ) for i in range(int(population_size_set-len(population_set)))]

    
    #print("Done creating children generation", generation)
    #create new population:
    population_set = population_set + randoms_set    
    
    print(generation_set, "done")     

    if not os.path.isfile('Results/'+str(out_put_filename)+'/'+str("result_"+out_put_filename+".csv")):
        print("in here")
        result_set.to_csv('Results/'+str(out_put_filename)+'/'+str("result_"+out_put_filename+".csv"), index=False)
               
    else:
        result_set.to_csv('Results/'+str(out_put_filename)+'/'+str("result_"+out_put_filename+".csv"), index=False, mode='a', header=False)


np.savetxt('Results/'+str(out_put_filename)+'/'+str("last_population_"+out_put_filename+".csv"), population_set, delimiter=",")
